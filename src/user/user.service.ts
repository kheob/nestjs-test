import { UserRepository } from './user.repository';
import { UserDto } from './user.dto';
import { User } from './user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: UserRepository,
  ) {}

  async create(userDto: UserDto): Promise<User> {
    const user = new User();
    user.username = userDto.email;
    user.password = userDto.password; // TODO: Need to hash this later
    return await this.userRepository.save(user);
  }
}
