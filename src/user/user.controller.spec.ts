import { UserService } from './user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserController } from './user.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from './user.entity';

describe('User Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useValue: {},
        },
      ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: UserController = module.get<UserController>(
      UserController,
    );
    expect(controller).toBeDefined();
  });
});
