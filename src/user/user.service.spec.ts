import { UserRepository } from './user.repository';
import { UserDto } from './user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('UserService', () => {
  let service: UserService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useClass: UserRepository,
        },
      ],
    }).compile();
    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('can create a user', async () => {
    const userDto = new UserDto();
    userDto.email = 'test@test.com';
    userDto.password = 'password123';
    const user = await service.create(userDto);
    expect(user.username).toEqual(userDto.email);
  });
});
