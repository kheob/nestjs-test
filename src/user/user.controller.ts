import { User } from './user.entity';
import { UserDto } from './user.dto';
import { UserService } from './user.service';
import { Controller, Post, Body, Param } from '@nestjs/common';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async create(@Body() userDto: UserDto): Promise<User> {
    return await this.userService.create(userDto);
  }
}
